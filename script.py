import pandas as pd
import re

filename = "source-file.xlsx"
allDFs=[]
# patterns to match iOS and Androids IDs
iosPat=re.compile("\((\d*)\)")
andPat=re.compile("\(([\w|.]*)\)")

def extractPlatform(ser,pattern):
    m = pattern.search(ser)
    if m:
        return m.group(1)
    else:
        return ""

myfile = pd.ExcelFile(filename)
for s in myfile.sheet_names:
    #Read a single sheet
    df = pd.read_excel(myfile,sheet_name=s)

    # Identifying the mobile platforms
    df["iOS"]=df["App/URL"].where(df["App/URL"].str.contains('iOS'),other="")
    df["Android"]=df["App/URL"].where(df["App/URL"].str.contains('Android'),other="")

    # Then for each, clean up the content to only have the application ID
    df["iOS"]=df["iOS"].apply(extractPlatform,args=(iosPat,))
    df["Android"]=df["Android"].apply(extractPlatform,args=(andPat,))

    # Finally reorder columns and add to the main data frame
    df=df[["Country","Android","iOS","App/URL","Potential Impressions","Unique Cookies with Impressions"]]
    allDFs.append(df)

result = pd.concat(allDFs)
result.to_csv("results.csv",index=False)
